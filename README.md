
# OpenCl Hello World for Cmake + VS-Code + Win10

## Prerequisites

(tested with Win 10 + Nvidia Drivers)

* Visual-Studio (Build-Tools at minimum) is installed
* OpenCL SDK for Nvidia

[Install vcpkg](https://docs.microsoft.com/en-Us/cpp/build/install-vcpkg?view=msvc-160&tabs=windows)

Add this to the VS-Code Workspace-Settings:

```json
{
    "cmake.configureSettings": {
        "CMAKE_TOOLCHAIN_FILE": "[rootpath of vcpkg]/vcpkg/scripts/buildsystems/vcpkg.cmake"
    }
}
```

Install opencl-headers:

```powershell
./vcpkg install opencl
```

## Build

(Note all this steps should be run on the 'Developer Command Prompt for VS 2019' 
to make sure all build-tols and Environment Variables are set properly)

```sh
mkdir build 
cd build
cmake ..
cmake --build .
```
